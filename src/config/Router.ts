 
import RouterAuth from '../modules/auth/RouterAuth';
import RouterHome from '../modules/home/RouterHome';
import RouterCompany from '../modules/company/RouterCompany';

export default [
  ...RouterAuth,
  ...RouterHome,
  ...RouterCompany,
];
