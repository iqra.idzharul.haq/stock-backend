 
import { Company } from './controllers';

const Routes: any = [
  {
    method: 'GET',
    path: '/company',
    config: {
      auth: false,
    },
    handler: (req: any, res: any): object => Company.getAllCompany(req, res),
  },
  {
    method: 'POST',
    path: '/company',
    config: {
      auth: false,
    },
    handler: (req: any, res: any): object => Company.saveCompany(req, res),
  },
];

export default Routes;
