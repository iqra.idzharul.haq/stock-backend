 
import { HttpResponse } from '../../../utilities';
import { Company } from '../../../model/entities';
import { getConnection } from 'typeorm';
import { validate } from 'class-validator';
class Companies {
  static getAllCompany = async (req: any, res: any): Promise<object> => { // eslint-disable-line @typescript-eslint/no-unused-vars
    try {
      let userRepository = getConnection().getRepository(Company);

      const company = await userRepository.find();
      if (company) {
        return res.response(HttpResponse(200, company));
      }
      return HttpResponse(401, 'no data');
    } catch (error) {
      console.log(error);
      if (error.message) return HttpResponse(400, error.message);
      return HttpResponse(500, error);
    }
  }
  static saveCompany = async (req: any, res: any): Promise<object> => { // eslint-disable-line @typescript-eslint/no-unused-vars
    try {
      let company = new Company();
      company.code = req.payload.code;
      company.name = req.payload.name;
      company.address = req.payload.address;
      company.founded   = req.payload.founded;
      company.email   = req.payload.email;

      const validation = await validate(company);
      if (validation.length) {
        let errors: any = Object.values(validation[0].constraints);
        throw errors;
      }

      let repository = getConnection().getRepository(Company);

      await repository.save(company);

      const companyData = await repository.findOne({
        where: {
          code: req.payload.code,
        },
      });

      if (companyData) return HttpResponse(201, companyData);
      return HttpResponse(204, {});
    } catch (error) {
      console.log(error);
      if (error.message) return HttpResponse(400, error.message);
      return HttpResponse(500, error);
    }
  }
}

export default Companies;
